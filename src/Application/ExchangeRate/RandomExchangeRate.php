<?php
namespace Application\ExchangeRate;

use Application\Exception\CurrencyPairNotSupported;
use Money\Currency;

class RandomExchangeRate implements ExchangeRateProvider
{
    public function fetch(Currency $currencyIn, Currency $currencyOut)
    {
		$seed = $currencyIn.''.$currencyOut;
		$xmlUrl = "http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.xchange%20where%20pair%20in(%22".$seed."%22)&env=store://datatables.org/alltableswithkeys";
		if ($xml = simplexml_load_file($xmlUrl)) 
		{
			$rate = (double)floatval($xml->results->rate->Rate);
			if($currencyIn.'/'.$currencyOut == $xml->results->rate->Name && $rate > 0)
			{
				return $rate;
			}
		}
		
        if ($currencyIn->getCode() === 'GBP' && $currencyOut->getCode() === 'PLN') {
            return round(4 + rand() / getrandmax(), 4);
        }

        if ($currencyIn->getCode() === 'GBP' && $currencyOut->getCode() === 'EUR') {
            return round(1 + rand() / getrandmax(), 4);
        }

        if ($currencyIn->getCode() === 'PLN' && $currencyOut->getCode() === 'EUR') {
            return round(rand() / getrandmax(), 4);
        }

        if ($currencyIn->getCode() === 'PLN' && $currencyOut->getCode() === 'GBP') {
            return round(rand() / getrandmax(), 4);
        }

        if ($currencyIn->getCode() === 'EUR' && $currencyOut->getCode() === 'PLN') {
            return round(4 + rand() / getrandmax(), 4);
        }

        throw new CurrencyPairNotSupported();
    }
	
	public function stringCurrencyValid(string $cunnency1, string $cunnency2)
	{
		// to set in base
		$arrayCurrency = array('gbp-pln', 'gbp-eur', 'pln-eur', 'pln-gbp', 'eur-pln');
		return in_array($cunnency1.'-'.$cunnency2, $arrayCurrency) ? TRUE : FALSE;
		
		throw new CurrencyPairNotSupported();
	}
	
	public function stringCurrencySymbol(string $cunnency)
	{
		// to set in base
		$arrayCurrencySymbols = array( 'gbp'=>'£', 'pln'=>'zł', 'eur'=>'€', 'usd'=>'$');
		return array_key_exists($cunnency, $arrayCurrencySymbols) ? $arrayCurrencySymbols[$cunnency] : 'x';
	}
	
	public function stringCurrencyLangDetect(string $text)
	{
		// to set in base
		$stringCurrencyLangDetect = array( 
			'convert' => ['convert','przelicz','convertir'],
			'to' => ['to','na','a']
		);
		foreach($stringCurrencyLangDetect as $key => $row) if(in_array($text, $row)) return $key;
		return false;
	}
	
	public function stringCurrencyLangValid(array $textArray)
	{
		// static language word validation without word conjugation
		if($this->stringCurrencyLangDetect($textArray[0]) == 'convert' && $this->stringCurrencyLangDetect($textArray[1]) == 'to')
		{
			return true;
		}		
		return false;
	}
}
