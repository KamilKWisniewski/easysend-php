<?php

namespace Application\ExchangeRate;

use Money\Currency;

/**
 * Interface ExchangeRateProvider
 */
interface ExchangeRateProvider
{
    public function fetch(Currency $currencyIn, Currency $currencyOut);
	public function stringCurrencyValid(string $cunnency1, string $cunnency2);
	public function stringCurrencySymbol(string $cunnency);
	public function stringCurrencyLangDetect(string $text);
	
}
