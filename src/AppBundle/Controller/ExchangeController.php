<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Money\Currency;
use Application\ExchangeRate\ExchangeRateProvider;
use Application\ExchangeRate\RandomExchangeRate;

class ExchangeController extends Controller
{
    /**
     * @Route("/exchange", name="exchange")
     */
    public function indexAction(Request $request)
    {
		$regDataString = $request->request->get('calculationForm')['convert'];
		// calculate area 
		// convert GBP 100 to PLN
		
		$currencyInputAmount = 0;
		$currencyOutputAmount = 0;
		$currencyRate = 0;
		$valueCurrency = 0;
		$errorString = '';
		$currencySymbolIn = '';
		$currencySymbolOut = '';
		
		$regDataArray = explode(' ', strtolower($regDataString) );
		if($this->get('exchange_rate.provider')->stringCurrencyLangValid([$regDataArray[0],$regDataArray[3]]))
		{
			$currencyStringIn = $regDataArray[1];
			$currencyStringOut = $regDataArray[4];
			if($this->get('exchange_rate.provider')->stringCurrencyValid($currencyStringIn,$currencyStringOut))
			{
				$currencyInputAmount = $regDataArray[2];
				if(is_numeric($currencyInputAmount))
				{
					$currencyIn = new Currency(strtoupper($currencyStringIn));
					$currencyOut = new Currency(strtoupper($currencyStringOut)); 
					$currencySymbolIn = $this->get('exchange_rate.provider')->stringCurrencySymbol($currencyStringIn);
					$currencySymbolOut = $this->get('exchange_rate.provider')->stringCurrencySymbol($currencyStringOut);
					$currencyRate = $this->get('exchange_rate.provider')->fetch($currencyIn, $currencyOut);	
					$currencyOutputAmount = $currencyInputAmount * $currencyRate;
				}
				else
				{
					$errorString = 'Currency not numeric';
				}
			}
			else
			{
				$errorString = 'Not supported currency exchange';
			}
		}
		else
		{
			$errorString = 'Santax input error';
		}
		
		$currencyInputAmountSplit = explode('.',number_format($currencyInputAmount, 2, '.', ','));
		$currencyOutputAmountSplit = explode('.',number_format($currencyOutputAmount, 2, '.', ','));
		
        return $this->render(
			'exchange/index.html.twig', 
			[
				'inputData' => $regDataString,
				'currencyError' => $errorString,
				'dataRate' => $currencyRate,
				'currencySymbolIn' => $currencySymbolIn,
				'currencySymbolOut' => $currencySymbolOut,
				
				'dataValueIn' => $currencyInputAmount,
				'dataValueOut' => $currencyOutputAmount,
				
				'dataValueInInt' => $currencyInputAmountSplit[0],
				'dataValueOutInt' => $currencyOutputAmountSplit[0],
				
				'dataValueInDou' => $currencyInputAmountSplit[1],
				'dataValueOutDou' => $currencyOutputAmountSplit[1]				
			] 
			);
			
    }
	
}
